# Exercise 3

## Enhanced Task Manager
Add additional functionality to the application that you'll have   created:
1. Add one more logical functionality (e. g. task assigned to a group of users 
2. Use an Automated Build Tool (Maven/Gradle)
3. Use Database instead of storing data in a file
4. Cover your code with JUnit tests


## New features
New logical functionality added to the application:
1. -deleteUser -un='UserName'
2. -completeTask -un='UserName' -tt='TaskTitle'
3. -completeAllTasks -un='UserName'
