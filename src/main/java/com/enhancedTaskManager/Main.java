package com.enhancedTaskManager;

public class Main {
    public static void main(String[] args) {
        CommandLineParser clp = new CommandLineParser(args);
        UserManager um = new UserManager(clp);

        boolean isCreateUser = clp.getFlag("createUser");
        boolean isShowAllUsers = clp.getFlag("showAllUsers");
        boolean isAddTask = clp.getFlag("addTask");
        boolean isShowTasks = clp.getFlag("showTasks");
        boolean isDeleteUser = clp.getFlag("deleteUser");
        boolean isCompleteTask = clp.getFlag("completeTask");
        boolean isCompleteAllTasks = clp.getFlag("completeAllTasks");

        if (isCreateUser) {
            um.createUser();
        }

        if (isShowAllUsers) {
            um.showAllUsers();
        }

        if (isAddTask) {
            um.addTask();
        }

        if (isShowTasks) {
            um.showTasks();
        }

        if (isDeleteUser) {
            um.deleteUser();
        }

        if (isCompleteTask) {
            um.completeTask();
        }

        if (isCompleteAllTasks) {
            um.completeAllTasks();
        }

    }
}
