package com.enhancedTaskManager;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable{
    private String firstName;
    private String lastName;
    private String userName;
    private ArrayList <Task> tasks = new ArrayList<>();

    public User(String firstName, String lastName, String userName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public ArrayList <Task> getTasks() {
        return tasks;
    }

    @Override
    public String toString() {
        return firstName + " : " + lastName + " : " + userName;
    }

    public void addTask(String taskTitle, String taskDescription) {
        this.tasks.add(new Task(taskTitle, taskDescription));
    }

    public void showTasks(){
        for (Task task: this.tasks) {
            System.out.println(task);
        }
    }
}
