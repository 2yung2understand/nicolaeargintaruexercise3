package com.enhancedTaskManager;

import java.sql.*;

public class UserManager {

    CommandLineParser clp;
    Connection connect;
    Statement statement;
    PreparedStatement preparedStatement;
    ResultSet resultSet;
    String url = "jdbc:mysql://localhost:3306/taskmanager";
    String user = "root";
    String pass = "root";

    UserManager(CommandLineParser clp) {
        this.clp = clp;
    }

    public void getUserId() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            System.out.println(preparedStatement.toString());
            resultSet = preparedStatement.executeQuery();
            int userid;
            while (resultSet.next()) {
                userid = resultSet.getInt("userid");
                System.out.println(userid);
            }
            resultSet.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void createUser() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("INSERT INTO users (firstname, lastname, username) VALUES (?, ?, ?);");
            preparedStatement.setString(1, clp.getArgumentValue("fn"));
            preparedStatement.setString(2, clp.getArgumentValue("ln"));
            preparedStatement.setString(3, clp.getArgumentValue("un"));
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void showAllUsers() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM users;");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("firstname") + " : " + resultSet.getString("lastname") + " : " + resultSet.getString("username"));
            }
            resultSet.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void addTask() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            resultSet = preparedStatement.executeQuery();
            int userId = 0;
            while (resultSet.next()) {
                userId = resultSet.getInt("userid");
            }
            preparedStatement = connect.prepareStatement("INSERT INTO tasks VALUES (?, ?, ?);");
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, clp.getArgumentValue("tt"));
            preparedStatement.setString(3, clp.getArgumentValue("td"));
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void showTasks() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            resultSet = preparedStatement.executeQuery();
            int userId = 0;
            while (resultSet.next()) {
                userId = resultSet.getInt("userid");
            }
            preparedStatement = connect.prepareStatement("SELECT tasktitle, taskdescription FROM tasks WHERE userid = ?;");
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println("Task title: " + resultSet.getString("tasktitle"));
                System.out.println("Task description" + resultSet.getString("taskkdescription"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void deleteUser() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            resultSet = preparedStatement.executeQuery();
            int userId = 0;
            while (resultSet.next()) {
                userId = resultSet.getInt("userid");
            }
            preparedStatement = connect.prepareStatement("DELETE FROM tasks WHERE userid = ?");
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();
            preparedStatement = connect.prepareStatement("DELETE FROM users WHERE userid = ?;");
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void completeAllTasks() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            resultSet = preparedStatement.executeQuery();
            int userId = 0;
            while (resultSet.next()) {
                userId = resultSet.getInt("userid");
            }
            preparedStatement = connect.prepareStatement("DELETE FROM tasks WHERE userid = ?;");
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

    public void completeTask() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            preparedStatement = connect.prepareStatement("SELECT userid FROM users WHERE username = ?");
            preparedStatement.setString(1, clp.getArgumentValue("un"));
            resultSet = preparedStatement.executeQuery();
            int userId = 0;
            while (resultSet.next()) {
                userId = resultSet.getInt("userid");
            }
            preparedStatement = connect.prepareStatement("DELETE FROM tasks WHERE userid = ? AND tasktitle = ?;");
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, clp.getArgumentValue("tt"));
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try { resultSet.close(); } catch (Exception e) { /* Ignored */ }
            try { preparedStatement.close(); } catch (Exception e) { /* Ignored */ }
            try { connect.close(); } catch (Exception e) { /* Ignored */ }
        }
    }

}
