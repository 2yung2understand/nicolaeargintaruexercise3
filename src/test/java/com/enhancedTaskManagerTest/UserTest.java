package com.enhancedTaskManagerTest;

import com.enhancedTaskManager.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    User user;

    @BeforeEach
    void setUser() {
        user = new User("FirstName", "LastName", "UserName");
    }

    @Test
    void testGetFirstName() {
        assertEquals("FirstName", user.getFirstName());
    }

    @Test
    void testGetLastName() {
        assertEquals("LastName", user.getLastName());
    }

    @Test
    void testGetUserName() {
        assertEquals("UserName", user.getUserName());
    }

    @Test
    void testToString() {
        assertEquals("FirstName : LastName : UserName", user.toString());
    }

}
