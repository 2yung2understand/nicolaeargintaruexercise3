package com.enhancedTaskManagerTest;

import com.enhancedTaskManager.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TaskTest {

    Task task;

    @BeforeEach
    void setTask() {
        task = new Task("TaskTitle", "TaskDescription");
    }

    @Test
    void testGetTaskTitle() {
        assertEquals("TaskTitle", task.getTaskTitle());
    }

    @Test
    void testGetTaskDescription() {
        assertEquals("TaskDescription", task.getTaskDescription());
    }

    @Test
    void testToString() {
        assertEquals("TaskTitle : TaskDescription", task.toString());
    }

}
